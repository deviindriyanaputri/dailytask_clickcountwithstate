import { useState } from "react"
import { PlusOutlined } from '@ant-design/icons';
import { MinusOutlined } from '@ant-design/icons';
import './App.css';

function App() {
  const [count, setCount] = useState(0);
  const [choose, setOne] = useState("Choose")

  return (
    <div className="App">
      <header className="App-header">
        <div>
          <h2>Try Click!!!</h2>
          <h3>{count}</h3>
          <button type="" className="App-button" onClick={() => setCount(count + 1)}>{<PlusOutlined />}</button>
          &nbsp;
          <button type="" className="App-button" onClick={() => setCount(count - 1)}>{<MinusOutlined />}</button>
        </div>

        <div>
          <h2>Follow???</h2>
          <h3>{choose}</h3>
          <button className="App-button" onClick={() => setOne('Following You')}>Follow</button>
          &nbsp;
          <button className="App-button" onClick={() => setOne("Don't Follow! Oge")}>Unfollow</button>
        </div>
      </header>
    </div>
  );
}

export default App;
